# Image to Handwritten text

This application Extract the text from image using Google ML kit

# Feature

* Extract the Text form Image
* Change the extracted text into Human hand written text
* Add multiple Images
* Create Pdf 
* Multiple color 
* Fully customizable style

# Demo

![Image to text](https://user-images.githubusercontent.com/48326144/235318259-89ef8aff-9f48-44eb-b5b4-d438b6cec306.gif)     



https://user-images.githubusercontent.com/48326144/235318294-da7c3a23-5132-40e1-9082-75fce2a3c56f.mov

